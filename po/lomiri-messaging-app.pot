# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-messaging-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: lomiri-messaging-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-03 08:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: lomiri-messaging-app.desktop.in:4
msgid "Messaging"
msgstr ""

#: lomiri-messaging-app.desktop.in:5
msgid "Messaging App"
msgstr ""

#: lomiri-messaging-app.desktop.in:6 src/messagingapplication.cpp:208
msgid "Messaging application"
msgstr ""

#: lomiri-messaging-app.desktop.in:7
msgid "Messages;SMS;MMS;Text Messages;Text"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:89
#, qt-format
msgid "%1 was invited to this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:91
#, qt-format
msgid "You invited %1 to this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:93
#, qt-format
msgid "%1 invited %2 to this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:96
#, qt-format
msgid "You switched to %1 @ %2"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:100
msgid "You left this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:103
#, qt-format
msgid "Renamed group to: %1"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:105
#, qt-format
msgid "You renamed group to: %1"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:107
#, qt-format
msgid "%1 renamed group to: %2"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:111
#, qt-format
msgid "%1 removed %2 from this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:113
#, qt-format
msgid "%1 left this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:116
msgid "You joined this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:119
#, qt-format
msgid "%1 added %2 to this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:121
#, qt-format
msgid "%1 joined this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:125
#, qt-format
msgid "%1 set %2 as Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:127
#, qt-format
msgid "%1 is Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:130
msgid "You are Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:133
#, qt-format
msgid "%1 set %2 as not Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:135
#, qt-format
msgid "%1 is not Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:138
msgid "You are not Admin"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:140
msgid "You were removed from this group"
msgstr ""

#: src/qml/AccountSectionDelegate.qml:142 src/qml/GroupChatInfoPage.qml:240
msgid "Group has been dissolved"
msgstr ""

#: src/qml/AttachmentDelegates/ContactDelegate.qml:48
#: src/qml/ThumbnailContact.qml:44
msgid "Unknown contact"
msgstr ""

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:28
msgid "Audio attachment not supported"
msgstr ""

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:31
msgid "Video attachment not supported"
msgstr ""

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:33
msgid "File type not supported"
msgstr ""

#: src/qml/AttachmentDelegates/PreviewerImage.qml:36
msgid "Image Preview"
msgstr ""

#: src/qml/AttachmentDelegates/PreviewerMultipleContacts.qml:91
#: src/qml/AttachmentDelegates/Previewer.qml:86
#: src/qml/MessagingContactEditorPage.qml:49
msgid "Save"
msgstr ""

#: src/qml/AttachmentDelegates/PreviewerMultipleContacts.qml:99
#: src/qml/AttachmentDelegates/Previewer.qml:93
#: src/qml/MessagingContactViewPage.qml:85
msgid "Share"
msgstr ""

#: src/qml/AttachmentDelegates/Previewer.qml:62
#: src/qml/Dialogs/EmptyGroupWarningDialog.qml:40
#: src/qml/Dialogs/MMSBroadcastDialog.qml:45
#: src/qml/Dialogs/MMSEnableDialog.qml:46
#: src/qml/Dialogs/RemoveThreadDialog.qml:43
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:39
#: src/qml/GroupChatInfoPage.qml:396 src/qml/MainPage.qml:157
#: src/qml/MessagingContactEditorPage.qml:36 src/qml/NewRecipientPage.qml:168
#: src/qml/OnlineAccountsHelper.qml:73
msgid "Cancel"
msgstr ""

#: src/qml/AttachmentDelegates/PreviewerVideo.qml:32
msgid "Video Preview"
msgstr ""

#: src/qml/AttachmentPanel.qml:104
msgid "Image"
msgstr ""

#: src/qml/AttachmentPanel.qml:117
msgid "Video"
msgstr ""

#: src/qml/AttachmentPanel.qml:155
msgid "Contact"
msgstr ""

#: src/qml/AudioRecordingBar.qml:154
msgid "<<< Swipe to cancel"
msgstr ""

#: src/qml/ComposeBar.qml:203 src/qml/Dialogs/EmptyGroupWarningDialog.qml:48
#: src/qml/GroupChatInfoPage.qml:432 src/qml/GroupChatInfoPage.qml:436
#: src/qml/Stickers/StickersPicker.qml:118
msgid "Remove"
msgstr ""

#: src/qml/ComposeBar.qml:227
msgid "You have to press and hold the record icon"
msgstr ""

#: src/qml/ComposeBar.qml:569
msgid "Write a broadcast message..."
msgstr ""

#: src/qml/ComposeBar.qml:579
msgid "Write a message..."
msgstr ""

#: src/qml/ComposeBar.qml:624 src/qml/MessageAlertBubble.qml:191
#: src/qml/MessageDelegate.qml:161
msgid "MMS"
msgstr ""

#: src/qml/ContactSearchWidget.qml:44
msgid "Members:"
msgstr ""

#: src/qml/ContactSearchWidget.qml:67
msgid "Number or contact name"
msgstr ""

#: src/qml/dateUtils.js:44
msgid "Today"
msgstr ""

#: src/qml/dateUtils.js:49
msgid "Yesterday"
msgstr ""

#: src/qml/Dialogs/EmptyGroupWarningDialog.qml:30
#, qt-format
msgid ""
"Removing last member will cause '%1' to be dissolved. Would you like to "
"continue?"
msgstr ""

#: src/qml/Dialogs/FileSizeWarningDialog.qml:26
msgid "File size warning"
msgstr ""

#: src/qml/Dialogs/FileSizeWarningDialog.qml:37
msgid ""
"You are trying to send big files (over 300Kb). Some operators might not be "
"able to send it."
msgstr ""

#: src/qml/Dialogs/FileSizeWarningDialog.qml:45
#: src/qml/Dialogs/NoMicrophonePermission.qml:49
#: src/qml/Dialogs/SimLockedDialog.qml:49
msgid "Ok"
msgstr ""

#: src/qml/Dialogs/FileSizeWarningDialog.qml:59
msgid "Don't show again"
msgstr ""

#: src/qml/Dialogs/InformationDialog.qml:30
#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:47
#: src/qml/Dialogs/NoNetworkDialog.qml:32 src/qml/MessageInfoDialog.qml:167
msgid "Close"
msgstr ""

#: src/qml/Dialogs/MMSBroadcastDialog.qml:30
msgid "Multiple MMS Messages"
msgstr ""

#: src/qml/Dialogs/MMSBroadcastDialog.qml:31
msgid ""
"The content you are sending requires one MMS message per recipient, so you "
"might get charged for multiple messages.\n"
"Do you want to continue?"
msgstr ""

#: src/qml/Dialogs/MMSBroadcastDialog.qml:35
msgid "Send"
msgstr ""

#: src/qml/Dialogs/MMSEnableDialog.qml:30
msgid "MMS support required"
msgstr ""

#: src/qml/Dialogs/MMSEnableDialog.qml:31
msgid ""
"MMS support is required to send this message.\n"
"Do you want to enable it?"
msgstr ""

#: src/qml/Dialogs/MMSEnableDialog.qml:35
msgid "Enable"
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:27
msgid "Welcome to your Messaging app!"
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:38
msgid ""
"If you wish to edit your SIM and other mobile preferences, please visit <a "
"href=\"system_settings\">System Settings</a>."
msgstr ""

#: src/qml/Dialogs/NoMicrophonePermission.qml:26
msgid "No permission to access microphone"
msgstr ""

#: src/qml/Dialogs/NoMicrophonePermission.qml:37
msgid ""
"Please grant access on <a href=\"system_settings\">System Settings &gt; "
"Security &amp; Privacy</a>."
msgstr ""

#: src/qml/Dialogs/NoNetworkDialog.qml:28
msgid "No network"
msgstr ""

#: src/qml/Dialogs/NoNetworkDialog.qml:29
#, qt-format
msgid "There is currently no network on %1"
msgstr ""

#: src/qml/Dialogs/NoNetworkDialog.qml:29
msgid "There is currently no network."
msgstr ""

#: src/qml/Dialogs/RemoveThreadDialog.qml:28
msgid "Delete thread"
msgid_plural "Delete threads"
msgstr[0] ""
msgstr[1] ""

#: src/qml/Dialogs/RemoveThreadDialog.qml:30
#, qt-format
msgid "Delete this thread?"
msgid_plural "Delete %1 threads?"
msgstr[0] ""
msgstr[1] ""

#: src/qml/Dialogs/RemoveThreadDialog.qml:47 src/qml/MessageAlertBubble.qml:170
#: src/qml/MessageDelegate.qml:127 src/qml/NewGroupPage.qml:309
#: src/qml/RegularMessageDelegate_irc.qml:151 src/qml/ThreadDelegate.qml:179
msgid "Delete"
msgstr ""

#. TRANSLATORS: %1 refers to the SIM card name or account name
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:29
#, qt-format
msgid "Change all Messaging associations to %1?"
msgstr ""

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:47
msgid "Change"
msgstr ""

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:62
msgid "Don't ask again"
msgstr ""

#: src/qml/Dialogs/SimLockedDialog.qml:26
msgid "SIM Card is locked"
msgstr ""

#: src/qml/Dialogs/SimLockedDialog.qml:37
msgid ""
"Please unlock your SIM card to call or send a message. You can unlock your "
"SIM card from the Network Indicator at the top of the screen or by visiting "
"<a href=\"system_settings\">System Settings &gt; Security &amp; Privacy</a>."
msgstr ""

#: src/qml/EmptyState.qml:50
msgid "Compose a new message by swiping up from the bottom of the screen."
msgstr ""

#: src/qml/GroupChatInfoPage.qml:112
msgid "Leave channel"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:114
msgid "Leave group"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:120
msgid "Channel Info"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:122
msgid "Group Info"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:128
msgid "Successfully left channel"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:130
msgid "Successfully left group"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:136
msgid "Failed to leave channel"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:138
msgid "Failed to leave group"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:149
msgid "Me"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:182
msgid "End group"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:213 src/qml/GroupChatInfoPage.qml:219
#: src/qml/GroupChatInfoPage.qml:225 src/qml/NewGroupPage.qml:59
msgid "This recipient was already selected"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:238
msgid "Failed to delete group"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:248
msgid "Failed to modify group title"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:374
#, qt-format
msgid "Participants: %1"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:374
msgid "Add participant:"
msgstr ""

#: src/qml/GroupChatInfoPage.qml:396
msgid "Add..."
msgstr ""

#: src/qml/ListItemDemo.qml:58 src/qml/PinchToZoomDemo.qml:76
msgid "Welcome to your Ubuntu Touch messaging app."
msgstr ""

#: src/qml/ListItemDemo.qml:139 src/qml/PinchToZoomDemo.qml:269
msgid "Got it"
msgstr ""

#: src/qml/ListItemDemo.qml:165
msgid "Swipe to reveal actions"
msgstr ""

#: src/qml/ListItemDemo.qml:215
msgid "Swipe to delete"
msgstr ""

#: src/qml/MainPage.qml:73 src/qml/Messages.qml:693
#: src/qml/NewRecipientPage.qml:130
msgid "Search..."
msgstr ""

#: src/qml/MainPage.qml:88
msgid "Messages"
msgstr ""

#: src/qml/MainPage.qml:111 src/qml/Messages.qml:897 src/qml/Messages.qml:1023
#: src/qml/NewRecipientPage.qml:141
msgid "Search"
msgstr ""

#: src/qml/MainPage.qml:121 src/qml/SettingsPage.qml:26
msgid "Settings"
msgstr ""

#: src/qml/MainPage.qml:130 src/qml/MainPage.qml:276
msgid "New message"
msgstr ""

#: src/qml/MainPage.qml:210
msgid "Select"
msgstr ""

#: src/qml/MainPage.qml:231
#, qt-format
msgid "%1 - Connecting..."
msgstr ""

#: src/qml/MessageAlertBubble.qml:34
msgid ""
"Could not fetch the MMS message. Maybe the MMS settings are incorrect or "
"cellular data is off? Ask to have the message sent again if everything is OK."
msgstr ""

#: src/qml/MessageAlertBubble.qml:35
msgid ""
"Could not fetch the MMS message. Maybe the MMS settings are incorrect or "
"cellular data is off?"
msgstr ""

#: src/qml/MessageAlertBubble.qml:36
#, qt-format
msgid "New MMS message (of %1 kB) to be downloaded before %2"
msgstr ""

#: src/qml/MessageAlertBubble.qml:145
msgid "Download"
msgstr ""

#: src/qml/MessageAlertBubble.qml:179 src/qml/MessageDelegate.qml:159
#: src/qml/ParticipantInfoPage.qml:35
#: src/qml/RegularMessageDelegate_irc.qml:186
msgid "Info"
msgstr ""

#: src/qml/MessageAlertBubble.qml:200 src/qml/MessageDelegate.qml:170
msgid "Myself"
msgstr ""

#: src/qml/MessageDelegate.qml:94 src/qml/RegularMessageDelegate_irc.qml:66
msgid "Text message copied to clipboard"
msgstr ""

#: src/qml/MessageDelegate.qml:136 src/qml/RegularMessageDelegate_irc.qml:163
msgid "Retry"
msgstr ""

#: src/qml/MessageDelegate.qml:144 src/qml/RegularMessageDelegate_irc.qml:171
msgid "Copy"
msgstr ""

#: src/qml/MessageDelegate.qml:152 src/qml/RegularMessageDelegate_irc.qml:179
msgid "Forward"
msgstr ""

#: src/qml/MessageDelegate.qml:161
msgid "SMS"
msgstr ""

#: src/qml/MessageInfoDialog.qml:54
msgid "Delivered"
msgstr ""

#: src/qml/MessageInfoDialog.qml:56
msgid "Temporarily Failed"
msgstr ""

#: src/qml/MessageInfoDialog.qml:58 src/qml/Messages.qml:320
msgid "Failed"
msgstr ""

#: src/qml/MessageInfoDialog.qml:60
msgid "Accepted"
msgstr ""

#: src/qml/MessageInfoDialog.qml:62 src/qml/MessageInfoDialog.qml:156
msgid "Read"
msgstr ""

#: src/qml/MessageInfoDialog.qml:64
msgid "Deleted"
msgstr ""

#: src/qml/MessageInfoDialog.qml:66 src/qml/ParticipantDelegate.qml:45
msgid "Pending"
msgstr ""

#: src/qml/MessageInfoDialog.qml:70 src/qml/MessageInfoDialog.qml:149
msgid "Received"
msgstr ""

#: src/qml/MessageInfoDialog.qml:72 src/qml/MessageInfoDialog.qml:75
#: src/qml/MessageInfoDialog.qml:91
msgid "Unknown"
msgstr ""

#: src/qml/MessageInfoDialog.qml:87
msgid "Group"
msgstr ""

#: src/qml/MessageInfoDialog.qml:95
msgid "Message info"
msgstr ""

#: src/qml/MessageInfoDialog.qml:98
msgid "Type"
msgstr ""

#: src/qml/MessageInfoDialog.qml:102
msgid "From"
msgstr ""

#: src/qml/MessageInfoDialog.qml:108
msgid "To"
msgstr ""

#: src/qml/MessageInfoDialog.qml:143
msgid "Sent"
msgstr ""

#: src/qml/MessageInfoDialog.qml:162
msgid "Status"
msgstr ""

#: src/qml/Messages.qml:99 src/qml/ThreadDelegate.qml:257
msgid "Cell Broadcast"
msgstr ""

#: src/qml/Messages.qml:311 src/qml/SendMessageValidator.qml:211
msgid "You have to disable flight mode"
msgstr ""

#: src/qml/Messages.qml:312 src/qml/SendMessageValidator.qml:212
msgid "It is not possible to send messages in flight mode"
msgstr ""

#: src/qml/Messages.qml:314 src/qml/SendMessageValidator.qml:214
msgid "No SIM card selected"
msgstr ""

#: src/qml/Messages.qml:315 src/qml/SendMessageValidator.qml:215
msgid "You need to select a SIM card"
msgstr ""

#: src/qml/Messages.qml:317 src/qml/SendMessageValidator.qml:217
msgid "No SIM card"
msgstr ""

#: src/qml/Messages.qml:318 src/qml/SendMessageValidator.qml:218
msgid "Please insert a SIM card and try again."
msgstr ""

#: src/qml/Messages.qml:321
msgid "It is not possible to send messages at the moment"
msgstr ""

#: src/qml/Messages.qml:331
msgid "Not available"
msgstr ""

#: src/qml/Messages.qml:332
msgid "The selected account is not available at the moment"
msgstr ""

#: src/qml/Messages.qml:515
msgid ""
"The SIM card does not provide the owner's phone number. Because of that "
"sending MMS group messages is not possible."
msgstr ""

#: src/qml/Messages.qml:672 src/qml/Messages.qml:748
#: src/qml/NewRecipientPage.qml:99 src/qml/NewRecipientPage.qml:367
#: src/qml/SettingsPage.qml:133 src/qml/SettingsPage.qml:287
msgid "Back"
msgstr ""

#: src/qml/Messages.qml:871
#, qt-format
msgid "Group (%1)"
msgstr ""

#: src/qml/Messages.qml:908 src/qml/Messages.qml:1034
msgid "Call"
msgstr ""

#: src/qml/Messages.qml:920
msgid "Add"
msgstr ""

#: src/qml/Messages.qml:1157
msgid "Create MMS Group..."
msgstr ""

#: src/qml/Messages.qml:1181
msgid "Join IRC Channel..."
msgstr ""

#: src/qml/Messages.qml:1187
#, qt-format
msgid "Create %1 Group..."
msgstr ""

#: src/qml/Messages.qml:1335
#, qt-format
msgid "%1 is typing..."
msgstr ""

#: src/qml/Messages.qml:1337
msgid "Typing..."
msgstr ""

#: src/qml/Messages.qml:1346
msgid "Online"
msgstr ""

#: src/qml/Messages.qml:1348
msgid "Offline"
msgstr ""

#: src/qml/Messages.qml:1350
msgid "Away"
msgstr ""

#: src/qml/Messages.qml:1352
msgid "Busy"
msgstr ""

#: src/qml/Messages.qml:1737
msgid ""
"You can't send messages to this group because the group is no longer active"
msgstr ""

#: src/qml/MessageStatusIcon.qml:75
msgid "Failed!"
msgstr ""

#: src/qml/MessagingBottomEdge.qml:26
msgid "+"
msgstr ""

#: src/qml/MessagingContactViewPage.qml:96
msgid "Edit"
msgstr ""

#: src/qml/MultiRecipientInput.qml:33
msgid "To:"
msgstr ""

#: src/qml/NewGroupPage.qml:81
msgid "Creating Group..."
msgstr ""

#: src/qml/NewGroupPage.qml:84
msgid "New MMS Group"
msgstr ""

#: src/qml/NewGroupPage.qml:88
msgid "Join IRC channel:"
msgstr ""

#: src/qml/NewGroupPage.qml:94
#, qt-format
msgid "New %1 Group"
msgstr ""

#: src/qml/NewGroupPage.qml:170
msgid "Failed to create group"
msgstr ""

#: src/qml/NewGroupPage.qml:232
msgid "Channel name:"
msgstr ""

#: src/qml/NewGroupPage.qml:234
msgid "Group name:"
msgstr ""

#: src/qml/NewGroupPage.qml:250
msgid "#channelName"
msgstr ""

#: src/qml/NewGroupPage.qml:252
msgid "Type a name..."
msgstr ""

#: src/qml/NewRecipientPage.qml:90
msgid "Add recipient"
msgstr ""

#: src/qml/NewRecipientPage.qml:114
msgid "All"
msgstr ""

#: src/qml/NewRecipientPage.qml:114
msgid "Favorites"
msgstr ""

#: src/qml/NewRecipientPage.qml:222
msgid "Contact Details"
msgstr ""

#: src/qml/NewRecipientPage.qml:286
msgid "Please select a phone number"
msgstr ""

#: src/qml/NewRecipientPage.qml:299
msgid "Numbers"
msgstr ""

#: src/qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr ""

#: src/qml/ParticipantDelegate.qml:42
msgid "Admin"
msgstr ""

#: src/qml/ParticipantInfoPage.qml:135
msgid "See in contacts"
msgstr ""

#: src/qml/ParticipantInfoPage.qml:135
msgid "Add to contacts"
msgstr ""

#: src/qml/ParticipantInfoPage.qml:153
msgid "Set as admin"
msgstr ""

#: src/qml/ParticipantInfoPage.qml:166
msgid "Send private message"
msgstr ""

#: src/qml/ParticipantInfoPage.qml:181
msgid "Remove from group"
msgstr ""

#: src/qml/PinchToZoomDemo.qml:66
msgid "Hello there!"
msgstr ""

#: src/qml/PinchToZoomDemo.qml:128
msgid "Pinch to decrease font size"
msgstr ""

#: src/qml/PinchToZoomDemo.qml:179
msgid "Spread to increase font size"
msgstr ""

#: src/qml/RegularMessageDelegate_irc.qml:188
msgid "IRC"
msgstr ""

#: src/qml/SendMessageValidator.qml:220
msgid "It is not possible to send the message"
msgstr ""

#: src/qml/SendMessageValidator.qml:221
msgid "Failed to send the message"
msgstr ""

#: src/qml/SettingsPage.qml:46
msgid "Sort by timestamp"
msgstr ""

#: src/qml/SettingsPage.qml:47
msgid "Sort by title"
msgstr ""

#: src/qml/SettingsPage.qml:51
msgid "System Theme"
msgstr ""

#: src/qml/SettingsPage.qml:52
msgid "Light"
msgstr ""

#: src/qml/SettingsPage.qml:53
msgid "Dark"
msgstr ""

#: src/qml/SettingsPage.qml:59
msgid "Enable MMS messages"
msgstr ""

#: src/qml/SettingsPage.qml:66
msgid "Enable stickers"
msgstr ""

#: src/qml/SettingsPage.qml:73
msgid "Simplified conversation view"
msgstr ""

#: src/qml/SettingsPage.qml:80
msgid "Sort threads"
msgstr ""

#: src/qml/SettingsPage.qml:88
msgid "AutoPlay animated image"
msgstr ""

#: src/qml/SettingsPage.qml:95
msgid "Auto display keyboard"
msgstr ""

#: src/qml/SettingsPage.qml:102
msgid "Theme"
msgstr ""

#: src/qml/Stickers/StickersPicker.qml:132
msgid "Stickers"
msgstr ""

#: src/qml/Stickers/StickersPicker.qml:133
msgid "Please confirm that you want to delete all stickers in this pack"
msgstr ""

#: src/qml/Stickers/StickersPicker.qml:133
msgid "Please confirm that you want to delete all stickers in the history"
msgstr ""

#: src/qml/Stickers/StickersPicker.qml:262
msgid "no stickers yet"
msgstr ""

#: src/qml/Stickers/StickersPicker.qml:303
msgid "sent stickers will appear here"
msgstr ""

#: src/qml/ThreadDelegate.qml:37
msgid "New MMS notification"
msgstr ""

#. TRANSLATORS: %1 is the first recipient the message is sent to, %2 is the count of remaining recipients
#: src/qml/ThreadDelegate.qml:78
#, qt-format
msgid "%1 + %2"
msgstr ""

#: src/qml/ThreadDelegate.qml:113
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] ""
msgstr[1] ""

#: src/qml/ThreadDelegate.qml:116
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] ""
msgstr[1] ""

#: src/qml/ThreadDelegate.qml:119
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] ""
msgstr[1] ""

#: src/qml/ThreadDelegate.qml:122
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] ""
msgstr[1] ""

#: src/qml/ThreadDelegate.qml:125
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] ""
msgstr[1] ""

#: src/qml/ThreadDelegate.qml:363
msgid "Draft:"
msgstr ""
