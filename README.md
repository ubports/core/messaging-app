# Lomiri Messaging App

Lomiri Messaging App is the official SMS app for Ubuntu Touch.

## Internals

Messaging app relies on:
 - [history-service](https://gitlab.com/ubports/development/core/lomiri-history-service) for the database backend through `Lomiri.History` QML import.
 - [telephony service](https://gitlab.com/ubports/development/core/lomiri-telephony-service) for message relay through `Lomiri.Telphony` QML import.
 - [address-book-app](https://gitlab.com/ubports/development/core/lomiri-address-book-app) for contact features through `Lomiri.Contacts` QML import.

## i18n: Translating Messaging App into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
[Hosted Weblate service](https://hosted.weblate.org/projects/lomiri/lomiri-messaging-app)

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

## Building with clickable

Note that it will not allow full feature access (url dispatcher, audio playback )

Install [clickable](https://clickable-ut.dev/en/latest/), then run:

```
clickable
```

For faster build speeds, building app tests is disabled in ```clickable.yaml``` 

## Building with crossbuilder ( build & install as a deb package )


Some dependencies need to be installed by running:

```
crossbuilder inst-foreign dh-translations apparmor-easyprof-ubuntu
```

The app then can be build by running:

```
crossbuilder
```

See [crossbuilder on github](https://github.com/ubports/crossbuilder) for details.

## Desktop local testings

If we want to test with real data, History database and Contact database must be present in `~/.local/share/history-service/history.sqlite` and `~/.local/share/evolution/addressbook/system/contacts.db` respectively

With `clickable ide qtcreator` , choose not to auto setup the project and select a classic Qt Kit, by running the project you should see your Threads and conversation.
Note that some features will not be available, i.e Contact Header, sending a sms, record and play audio, content hub

### Tests

Tests can be run in the build directory via terminal with `make test`


## Useful Links

Here are some useful links with regards to the Lomiri Messaging App development.

* [UBports](https://ubports.com/)
* [building with crossbuilder](https://docs.ubports.com/en/latest/systemdev/testing-locally.html#cross-building-with-crossbuilder)
* [crossbuilder on github](https://github.com/ubports/crossbuilder)
* [OpenStore](https://open-store.io/)
* [MMS infrastructure on Ubuntu Touch](http://docs.ubports.com/en/latest/systemdev/mms-infrastructure.html)
