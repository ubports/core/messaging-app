/*
 * Copyright 2022 Ubports Foundation
 *
 * This file is part of lomiri-messaging-app.
 *
 * lomiri-messaging-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * dialer-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Rectangle {
    id: root

    property bool enabled
    property var  mainView: root
    property int scaleLevel: 1
    readonly property variant fontSizeNames: ["small", "medium", "large", "x-large"]
    readonly property string scaledFontSize: fontSizeNames[scaleLevel]
    signal disable

    color: "black"
    opacity: 0.0
    Behavior on opacity {
        NumberAnimation {
            duration:  1600
        }
    }

    Rectangle {

        anchors {
            top: parent.top
            topMargin: units.gu(12)
            left: parent.left
            right: parent.right
        }
        color: "transparent"

        Column {
            id: messageList
            anchors.margins: units.gu(2)
            anchors {
                left: parent.left
                right: parent.right
            }

            spacing: units.gu(2)

            MessageBubble {
                id: incomingMessageBubble
                objectName: 'incomingMessageBubble'

                anchors.right: parent.right
                messageText: i18n.tr("Hello there!")
                messageTimeStamp: new Date()
                messageStatus:1
                messageIncoming: true
            }

            MessageBubble {
                id: outgoingMessageBubble
                objectName: 'outgoingMessageBubble'

                messageText: i18n.tr("Welcome to your Ubuntu Touch messaging app.")
                messageTimeStamp: new Date()
                messageStatus:1
                messageIncoming: false
            }
        }
    }


    ColumnLayout {
        id: column
        anchors {
            left: parent.left
            right: parent.right
            bottom: gotItButton.top
            margins: units.gu(1)
            bottomMargin: units.gu(3)
        }

        Label {
            id: dragMessage

            Layout.alignment: Qt.AlignHCenter
            height: parent.height
            color: "white"
            opacity: 0
            wrapMode: Text.Wrap
            fontSize: "large"
        }

        Image {
            id: gestureImage
            source: "image://theme/gestures"
            Layout.alignment: Qt.AlignHCenter
            fillMode: Image.Pad
            sourceSize {
                width: units.gu(16)
                height: units.gu(16)

            }
        }
    }

    SequentialAnimation {
        id: anim

        loops: Animation.Infinite
        running: root.enabled

        PropertyAction {
            target: dragMessage
            property: "text"
            value: i18n.tr("Pinch to decrease font size")
        }

        ParallelAnimation {
            PropertyAnimation {
                target: dragMessage
                property: "opacity"
                to: 0.8
                duration: 400
            }
            PropertyAnimation {
                target: gestureImage
                property: "opacity"
                to: 0.6
                duration: 400
            }
        }

        PauseAnimation {
            duration: 3000
        }

        PropertyAction {
            target: mainView
            property: "scaleLevel"
            value: 0
        }

        PauseAnimation {
            duration: 2000
        }


        ParallelAnimation {
            PropertyAnimation {
                target: dragMessage
                property: "opacity"
                to: 0
                duration: 1000
            }
            PropertyAnimation {
                target: gestureImage
                property: "opacity"
                to: 0
                duration: 1000
            }
        }

        PropertyAction {
            target: dragMessage
            property: "text"
            value: i18n.tr("Spread to increase font size")
        }

        ParallelAnimation {
            PropertyAnimation {
                target: dragMessage
                property: "opacity"
                to: 0.8
                duration: 400
            }
            PropertyAnimation {
                target: gestureImage
                property: "opacity"
                to: 0.6
                duration: 400
            }
        }

        PauseAnimation {
            duration: 1000
        }

        PropertyAction {
            target: mainView
            property: "scaleLevel"
            value: 1
        }

        PauseAnimation {
            duration: 1000
        }

        PropertyAction {
            target: mainView
            property: "scaleLevel"
            value: 2
        }

        PauseAnimation {
            duration: 1000
        }

        PropertyAction {
            target: mainView
            property: "scaleLevel"
            value: 3
        }

        PauseAnimation {
            duration: 3000
        }

        ParallelAnimation {
            PropertyAnimation {
                target: dragMessage
                property: "opacity"
                to: 0
                duration: 200
            }
            PropertyAnimation {
                target: gestureImage
                property: "opacity"
                to: 0
                duration: 200
            }
        }

        PropertyAction {
            target: mainView
            property: "scaleLevel"
            value: 1
        }

        PauseAnimation {
            duration: 2000
        }

    }

    Button {
        id: gotItButton
        objectName: "gotItButton"

        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            bottomMargin: units.gu(9)
        }
        width: units.gu(17)
        strokeColor: theme.palette.normal.positive
        text: i18n.tr("Got it")
        enabled: !dismissAnimation.running
        onClicked: dismissAnimation.start()

        InverseMouseArea {
            anchors.fill: parent
            topmostItem: false
        }
    }

    SequentialAnimation {
        id: dismissAnimation

        alwaysRunToEnd: true
        running: false

        LomiriNumberAnimation {
            target: root
            property: "opacity"
            to: 0.0
            duration:  LomiriAnimation.SlowDuration
        }
        ScriptAction {
            script: root.disable()
        }
    }

    Component.onCompleted: {
        opacity = 0.85
        pageHeader.visible = false
    }
}
